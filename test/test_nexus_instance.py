import inspect
import os
import unittest
from unittest import mock

import pytest

from hoppr.exceptions import HopprCredentialsError

from nexus_bundler.nexus_instance import NexusInstance


class TestNexusInstance(unittest.TestCase):
    @mock.patch.dict(os.environ, {"PW_ENV": "test-password"})
    def test_instance(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        instance = NexusInstance(
            url="http://my-nexus.org",
            userid="my-userid",
            password_env="PW_ENV",
        )

        assert instance.url == "http://my-nexus.org/"
        assert instance.userid == "my-userid"
        assert instance.password == "test-password"
        assert instance.docker_url == "http://my-nexus.org:5000/"

        assert instance.get_repository_api() == "http://my-nexus.org/service/rest/beta/repositories"
        assert instance.get_component_api() == "http://my-nexus.org/service/rest/v1/components"

    def test_instance_no_pw_env(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        with pytest.raises(HopprCredentialsError):
            instance = NexusInstance(
                url="http://my-nexus.org",
                userid="my-userid",
                password_env="PW_ENV",
            )
