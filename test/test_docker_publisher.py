import inspect
import json
import os
import unittest
import urllib

from copy import deepcopy
from pathlib import Path
from subprocess import CompletedProcess
from test.mock_objects import MockHttpResponse
from threading import _RLock as RLock
from unittest import mock

import pytest

from hoppr import HopprContext, Manifest, Result, Sbom
from hoppr.exceptions import HopprError

from nexus_bundler.bundler import NexusBundlePlugin
from nexus_bundler.nexus_instance import NexusInstance
from nexus_bundler.publishers.docker_publisher import DockerPublisher


class TestDockerPublisher(unittest.TestCase):

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.logfile_lock = RLock()

    def get_test_publisher(self):
        instance = NexusInstance(
            url="http://my-nexus.org",
            userid="my-userid",
            password_env="NEXUS_PW",
            docker_port=5000
        )

        manifest = Manifest.load(Path(__file__).parent / "resources" / "manifests" / "docker-manifest.yml")
        sbom_path = Path("test/resources/sboms/int_docker_bom.json")
        bom_dict = json.loads(sbom_path.read_text(encoding="utf-8"))
        sbom = Sbom(**bom_dict)

        context = HopprContext(
            collect_root_dir=Path("ROOT_DIR"),
            consolidated_sbom=sbom,
            credential_required_services=None,
            delivered_sbom=deepcopy(sbom),
            logfile_lock=self.logfile_lock,
            max_processes=3,
            repositories=manifest.repositories,
            sboms=list(Sbom.loaded_sboms.values()),
            stages=[],
        )

        bundler = NexusBundlePlugin(context)
        pub = DockerPublisher(instance, context, {"skopeo_command": "skopeo"})

        return pub, bundler

    repo_data = [
        { "name": "transfer-raw", "format": "raw"},
        { "name": "transfer-docker", "format": "docker"},
    ]

    @mock.patch("nexus_bundler.publishers.docker_publisher.open")
    @mock.patch.object(DockerPublisher, "run_command", return_value=CompletedProcess(args=['something'], returncode=0))
    @mock.patch("requests.post", return_value=MockHttpResponse(201, content="mocked content"))
    @mock.patch.object(DockerPublisher, "_has_good_repository", return_value=False)
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_success(self,
            mock_command_check,
            mock_has_good_repo,
            mock_request_post,
            mock_authentication,
            mock_open):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.push_artifact(Path(f"ROOT_DIR/docker/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        bundler.close_logger()
        pub.close_logger()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @mock.patch.object(DockerPublisher, "run_command", return_value=CompletedProcess(args=['something'], returncode=1))
    @mock.patch("requests.post", return_value=MockHttpResponse(201, content="mocked content"))
    @mock.patch.object(DockerPublisher, "_has_good_repository", return_value=False)
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    @mock.patch("nexus_bundler.publishers.docker_publisher.dist_version", return_value="v1.8.8.dev1")
    def test_prepare_push_fail(
        self,
        mock_command_check,
        mock_has_good_repo,
        mock_request_post,
        mock_authentication,
        mock_dist_version,
    ):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        pub, bundler = self.get_test_publisher()

        result = pub.push_artifact(
            Path(
                f"ROOT_DIR/docker/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more@version"
            )
        )
        bundler.close_logger()
        pub.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert (
            result.message
            == "Failure after 3 attempts, final message Skopeo failed to copy docker image to docker://my-nexus.org:5000/more:version, return_code=1"
        )

    @mock.patch.object(
        DockerPublisher,
        "run_command",
        return_value=CompletedProcess(args=["something"], returncode=1),
    )
    @mock.patch(
        "requests.post", return_value=MockHttpResponse(201, content="mocked content")
    )
    @mock.patch.object(DockerPublisher, "_has_good_repository", return_value=False)
    @mock.patch(
        "nexus_bundler.publishers.base_publisher.check_for_missing_commands",
        return_value=Result.success(),
    )
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    @mock.patch("nexus_bundler.publishers.docker_publisher.dist_version", return_value="1.8.5")
    def test_prepare_push_fail_old_version(
        self,
        mock_command_check,
        mock_has_good_repo,
        mock_request_post,
        mock_authentication,
        mock_dist_version,
    ):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")
        pub, bundler = self.get_test_publisher()

        result = pub.push_artifact(Path(f"ROOT_DIR/docker/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more_version"))
        bundler.close_logger()
        pub.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Failure after 3 attempts, final message Skopeo failed to copy docker image to docker://my-nexus.org:5000/more:version, return_code=1"

    @mock.patch("requests.put", return_value=MockHttpResponse(401))
    @mock.patch("requests.get", return_value=MockHttpResponse(200,json_data=[]))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_artifact_cant_create_token(self, mock_get, mock_put):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.prepare_push_artifact(Path(f"ROOT_DIR/docker/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"

    @mock.patch("requests.get", return_value=MockHttpResponse(404,json_data=[]))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_artifact_cant_retrieve_realms(self, mock_response):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.prepare_push_artifact(Path(f"ROOT_DIR/docker/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"

    @mock.patch("requests.post", return_value=MockHttpResponse(201, content="mocked content"))
    @mock.patch.object(DockerPublisher, "_has_good_repository", return_value=False)
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch("requests.get", return_value=MockHttpResponse(200,json_data=["DockerToken"]))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_artifact_success(self, mock_response, mock_command_check,
            mock_has_good_repo,
            mock_request_post):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.prepare_push_artifact(Path(f"ROOT_DIR/docker/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

        assert result.is_success()



    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_is_good_repo(self,
            mock_request_get):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()
        repo = {"name": "transfer-docker", "docker": {"httpPort" : 5000, "httpsPort" : None}}
        repo1 = {"name": "transfer-docker", "docker": {"httpPort" : None, "httpsPort" : 5000}}

        result = pub._is_good_repository(repo, Path("ROOT_DIR/docker/stuff"))
        result1 = pub._is_good_repository(repo1, Path("ROOT_DIR/docker/stuff"))
        pub.close_logger()
        bundler.close_logger()

        assert result == True
        assert result1 == True

    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_is_good_repo_bad_ports(self,
            mock_request_get):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()
        repo = {"name": "transfer-docker", "docker": {"httpPort" : None, "httpsPort" : None}}

        result = True
        with pytest.raises(HopprError):
            result = pub._is_good_repository(repo, Path("ROOT_DIR/docker/stuff"))
        pub.close_logger()
        bundler.close_logger()
        assert result == True

    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_is_good_repo_bad_name(self,
            mock_request_get):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()
        repo = {"name": "docker-transfer", "format" : "docker", "docker": {"httpPort" : 5000, "httpsPort" : 5000}}

        result = True
        with pytest.raises(HopprError):
            result = pub._is_good_repository(repo, Path("ROOT_DIR/docker/stuff"))
        pub.close_logger()
        bundler.close_logger()

        assert result == True

    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_is_good_repo_bad_format(self,
            mock_request_get):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()
        repo = {"name": "docker-transfer", "format" : "helm", "docker": {"httpPort" : 5000, "httpsPort" : 5000}}

        result = pub._is_good_repository(repo, Path("ROOT_DIR/docker/stuff"))
        pub.close_logger()
        bundler.close_logger()

        assert result == False

    @mock.patch("requests.get", return_value=MockHttpResponse(403, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_has_good_repo(self,
            mock_request_get):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler  = self.get_test_publisher()

        result = True
        with pytest.raises(HopprError):
            result = pub._has_good_repository(Path("ROOT_DIR/docker/stuff"))
        assert result == True

        result1 = True
        with pytest.raises(HopprError):
            result1 = pub._has_good_repository(Path("ROOT_DIR/DoCker/stuff"))
        assert result1 == True

        pub.close_logger()
        bundler.close_logger()

    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_build_repo_request_http(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub._build_repository_request(Path(f"ROOT_DIR/docker/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.nexus_instance.docker_url = "https://somewhere.com"
        result1 = pub._build_repository_request(Path(f"ROOT_DIR/docker/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

        port_type = "httpPort"
        assert result["docker"] == {"v1Enabled": True,
            "forceBasicAuth": False, port_type: pub.nexus_instance.docker_port}
        port_type = "httpsPort"
        assert result1["docker"] == {"v1Enabled": True,
            "forceBasicAuth": False, port_type: pub.nexus_instance.docker_port}