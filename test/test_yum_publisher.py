import inspect
import os
from copy import deepcopy
from pathlib import Path
from threading import _RLock as RLock
import unittest
import urllib
import json
from unittest import mock
from nexus_bundler.publishers.base_publisher import Publisher
from test.mock_objects import MockHttpResponse
from hoppr.exceptions import HopprPluginError

import pytest

from hoppr import HopprContext, Manifest, Result, Sbom

from nexus_bundler.bundler import NexusBundlePlugin
from nexus_bundler.nexus_instance import NexusInstance
from nexus_bundler.publishers.yum_publisher import YumPublisher

class TestYumPublisher(unittest.TestCase):

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.logfile_lock = RLock()

    def get_test_publisher(self):
        instance = NexusInstance(url="http://my-nexus.org", userid="my-userid", password_env="NEXUS_PW")
        manifest = Manifest.load(Path(__file__).parent / "resources" / "manifests" / "yum-manifest.yml")
        sbom_path = Path("test/resources/sboms/int_yum_bom.json")
        bom_dict = json.loads(sbom_path.read_text(encoding="utf-8"))
        sbom = Sbom(**bom_dict)

        context = HopprContext(
            collect_root_dir=Path("ROOT_DIR"),
            consolidated_sbom=sbom,
            credential_required_services=None,
            delivered_sbom=deepcopy(sbom),
            logfile_lock=self.logfile_lock,
            max_processes=3,
            repositories=manifest.repositories,
            sboms=list(Sbom.loaded_sboms.values()),
            stages=[],
        )

        bundler = NexusBundlePlugin(context)
        pub = YumPublisher(instance, context)

        return pub, bundler

    @mock.patch("nexus_bundler.publishers.yum_publisher.open")
    @mock.patch("requests.post", return_value=MockHttpResponse(201, content="mocked content"))
    @mock.patch.object(YumPublisher, "_has_good_repository", return_value=False)
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_push_success(self,
            mock_command_check,
            mock_has_good_repo,
            mock_request_post,
            mock_open):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.push_artifact(Path(f"ROOT_DIR/rpm/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        bundler.close_logger()
        pub.close_logger()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_get_repo_name(self):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        assert pub.get_repository_name(Path(f"ROOT_DIR/rpm/repo_dir/alpha/beta/Packages/gamma/file.rpm")) == "transfer-rpm--d2"
        pub.yum_data_depth = -1  # needed to re-run data-depth calculation
        assert pub.get_repository_name(Path(f"ROOT_DIR/rpm/repo_dir/alpha/beta/RPMS/gamma/file.rpm")) == "transfer-rpm--d2"
        pub.yum_data_depth = -1
        assert pub.get_repository_name(Path(f"ROOT_DIR/rpm/repo_dir/alpha/beta/gamma/file.rpm")) == "transfer-rpm--d3"

        bundler.close_logger()
        pub.close_logger()

    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_build_repo_request(self):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()
        pub.yum_data_depth = 7

        result = pub._build_repository_request(Path(f"ROOT_DIR/rpm/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

        assert result["yum"] == {"repodataDepth": 7, "deployPolicy": "STRICT"}

    @mock.patch.object(Publisher, "_is_good_repository", return_value=True)
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_is_good_repo(self, mock_base_is_good_repo):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()
        pub.yum_data_depth = 7
        repo = {
            "name": "test-yum-repo",
            "format": "yum",
            "yum": { "repodataDepth": 7 }
        }

        result = pub._is_good_repository(repo, Path(f"ROOT_DIR/rpm/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

        assert result == True

    @mock.patch.object(Publisher, "_is_good_repository", return_value=True)
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_is_not_good_repo(self, mock_base_is_good_repo):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()
        pub.yum_data_depth = 7
        repo = {
            "name": "test-yum-repo",
            "format": "yum",
            "yum": { "repodataDepth": 42 }
        }

        with pytest.raises(HopprPluginError):
            result = pub._is_good_repository(repo, Path(f"ROOT_DIR/rpm/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

    @mock.patch.object(Publisher, "_is_good_repository", return_value=False)
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_is_not_good_base_repo(self, mock_base_is_good_repo):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()
        pub.yum_data_depth = 7
        repo = {
            "name": "test-yum-repo",
            "format": "yum",
            "yum": { "repodataDepth": 7 }
        }

        result = pub._is_good_repository(repo, Path(f"ROOT_DIR/rpm/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

        assert result == False

    @mock.patch("requests.get", return_value=MockHttpResponse(400))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_finalize_no_good_repos(self, mock_request_get):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.finalize()
        pub.close_logger()
        bundler.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"

    @mock.patch("requests.post", return_value=MockHttpResponse(401))
    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=[
        {"name": "test-yum-repo--d3", "format": "yum"},
        {"name": "test-yum-repo--d5", "format": "yum"},
        {"name": "test-raw-repo", "format": "raw"},
    ]))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_finalize_bad_group_post(self, mock_request_get,
        mock_request_put):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.finalize()
        pub.close_logger()
        bundler.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"

    @mock.patch("requests.post", return_value=MockHttpResponse(201))
    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=[
        {"name": "test-yum-repo--d3", "format": "yum"},
        {"name": "test-yum-repo--d5", "format": "yum"},
        {"name": "test-raw-repo", "format": "raw"},
    ]))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_finalize_success(self, mock_request_get,
        mock_request_put):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.finalize()
        pub.close_logger()
        bundler.close_logger()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"
