import inspect
import os
from copy import deepcopy
from pathlib import Path
from subprocess import CompletedProcess
from threading import _RLock as RLock
import unittest
import urllib
import json
from unittest import mock
from hoppr.exceptions import HopprError
from test.mock_objects import MockHttpResponse, MockSubprocessRun

import pytest
from hoppr import HopprContext, Manifest, Result, Sbom

from nexus_bundler.bundler import NexusBundlePlugin
from nexus_bundler.nexus_instance import NexusInstance
from nexus_bundler.publishers.base_publisher import Publisher

class TestBasePublisher(unittest.TestCase):

    class TestPublisher(Publisher):

        required_tools = ["test-tool"]

        def push_artifact(self, path: Path) -> Result:
            return Result.success()

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.logfile_lock = RLock()

    def get_test_publisher(self):
        instance = NexusInstance(url="http://my-nexus.org", userid="my-userid", password_env="NEXUS_PW")
        manifests_dir = Path(__file__).parent / "resources" / "manifests"

        sbom_path = Path("test/resources/sboms/int_apt_bom.json")
        bom_dict = json.loads(sbom_path.read_text(encoding="utf-8"))
        sbom = Sbom(**bom_dict)

        manifest = Manifest.load(
            {
                "schemaVersion": "v1",
                "kind": "Manifest",
                "metadata": {
                    "name": "Base Publisher Test Manifest",
                    "version": "0.1.0",
                    "description": "A manifest for testing base publisher.",
                },
                "sboms": [],
                "includes": [
                    {"local": manifests_dir / "apt-manifest.yml"},
                    {"local": manifests_dir / "docker-manifest.yml"},
                    {"local": manifests_dir / "git-manifest.yml"},
                    {"local": manifests_dir / "helm-manifest.yml"},
                    {"local": manifests_dir / "maven-manifest.yml"},
                    {"local": manifests_dir / "pypi-manifest.yml"},
                    {"local": manifests_dir / "raw-manifest.yml"},
                    {"local": manifests_dir / "yum-manifest.yml"},
                ],
                "repositories": {},
            }
        )

        context = HopprContext(
            collect_root_dir=Path("ROOT_DIR"),
            consolidated_sbom=sbom,
            credential_required_services=None,
            delivered_sbom=deepcopy(sbom),
            logfile_lock=self.logfile_lock,
            max_processes=3,
            repositories=manifest.repositories,
            sboms=list(Sbom.loaded_sboms.values()),
            stages=[],
        )

        bundler = NexusBundlePlugin(context)
        pub = self.TestPublisher(instance, context)

        return pub

    repo_data = [
        { "name": "transfer-raw", "format": "raw"},
        { "name": "transfer-docker", "format": "docker"},
    ]

    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_get_repo_name(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()
        pub.close_logger()

        assert pub.get_repository_name(Path(f"ROOT_DIR/purl_type/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more")) == "theRepoName"

    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.fail("Tool not found"))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_missing_tool(self, mock_command_check):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub.prepare_push_artifact(Path(f"ROOT_DIR/purl_type/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Tool not found"

    @mock.patch.object(Publisher, "_has_good_repository", side_effect=HopprError("Cannot create repo"))
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_cannot_build_repo(self, mock_command_check, mock_has_good_repo):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub.prepare_push_artifact(Path(f"ROOT_DIR/purl_type/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Cannot create repo"

    @mock.patch("requests.post", return_value=MockHttpResponse(201, content="mocked content"))
    @mock.patch.object(Publisher, "_has_good_repository", return_value=False)
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_publish_success(self,
            mock_command_check,
            mock_has_good_repo,
            mock_request_post):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub.publish(Path(f"ROOT_DIR/purl_type/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @mock.patch("requests.post", return_value=MockHttpResponse(501, content="mocked content"))
    @mock.patch.object(Publisher, "_has_good_repository", return_value=False)
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_repo_fail(self,
            mock_command_check,
            mock_has_good_repo,
            mock_request_post):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub.prepare_push_artifact(Path(f"ROOT_DIR/purl_type/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Failure after 3 attempts, final message Response code 501 returned from nexus API call to create raw repository 'theRepoName'.\nResponse text: "

    @mock.patch.object(Path, "is_file", return_value=True)
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_process_at(self,
            mock_is_file):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub.should_process_at(Path(f"ROOT_DIR/purl_type/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()

        assert result == True

    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_finalize(self):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub.finalize()
        pub.close_logger()

        assert result.is_skip(), f"Expected SKIP result, got {result}"

    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_has_good_repo(self,
            mock_request_get):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub._has_good_repository(Path("ROOT_DIR/raw/stuff"))
        pub.close_logger()

        assert result == True

    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_has_good_repo_not(self,
            mock_request_get):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub._has_good_repository(Path("ROOT_DIR/pip/stuff"))
        pub.close_logger()

        assert result == False

    @mock.patch("requests.get", return_value=MockHttpResponse(403, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_has_good_repo_bad_get(self,
            mock_request_get):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        with pytest.raises(HopprError):
            result = pub._has_good_repository(Path("ROOT_DIR/raw/stuff"))

        pub.close_logger()

    @mock.patch("requests.get", return_value=MockHttpResponse(200, json_data=repo_data))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_has_bad_repo(self,
            mock_request_get):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()
        repo = {"name": "transfer-raw", "format": "maven2"}

        with pytest.raises(HopprError):
            result = pub._is_good_repository(repo, Path("ROOT_DIR/raw/stuff"))
        pub.close_logger()

    @mock.patch("requests.post", return_value=MockHttpResponse(500))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_upload_component(self,
            mock_request_post):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub.upload_component("transfer-raw")
        pub.close_logger()

        assert result.is_retry(), f"Expected RETRY result, got {result}"

    @mock.patch("hoppr.utils.obscure_passwords", return_value="something")
    @mock.patch("subprocess.run", return_value=MockSubprocessRun(1))
    @mock.patch("requests.post", return_value=MockHttpResponse(500))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_run_component_success(self,
            mock_request_post,
            mock_return_code,
            mock_obscure_pass
            ):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub = self.get_test_publisher()

        result = pub.run_command("command")
        pub.close_logger()

        assert result.stdout == b"Sample Standard Output"
