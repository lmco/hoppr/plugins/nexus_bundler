import inspect
import os
from copy import deepcopy
from pathlib import Path
from threading import _RLock as RLock
import unittest
import urllib
import json
from unittest import mock
from test.mock_objects import MockHttpResponse

from hoppr import HopprContext, Manifest, Result, Sbom

from nexus_bundler.bundler import NexusBundlePlugin
from nexus_bundler.nexus_instance import NexusInstance
from nexus_bundler.publishers.helm_publisher import HelmPublisher

class TestHelmPublisher(unittest.TestCase):

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.logfile_lock = RLock()

    def get_test_publisher(self):
        instance = NexusInstance(url="http://my-nexus.org", userid="my-userid", password_env="NEXUS_PW")
        manifest = Manifest.load(Path(__file__).parent / "resources" / "manifests" / "helm-manifest.yml")
        sbom_path = Path("test/resources/sboms/int_helm_bom.json")
        bom_dict = json.loads(sbom_path.read_text(encoding="utf-8"))
        sbom = Sbom(**bom_dict)

        context = HopprContext(
            collect_root_dir=Path("ROOT_DIR"),
            consolidated_sbom=sbom,
            credential_required_services=None,
            delivered_sbom=deepcopy(sbom),
            logfile_lock=self.logfile_lock,
            max_processes=3,
            repositories=manifest.repositories,
            sboms=list(Sbom.loaded_sboms.values()),
            stages=[],
        )

        bundler = NexusBundlePlugin(context)
        pub = HelmPublisher(instance, context)

        return pub, bundler

    @mock.patch("nexus_bundler.publishers.helm_publisher.open")
    @mock.patch("requests.post", return_value=MockHttpResponse(201, content="mocked content"))
    @mock.patch.object(HelmPublisher, "_has_good_repository", return_value=False)
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_success(self,
            mock_command_check,
            mock_has_good_repo,
            mock_request_post,
            mock_open):
            
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.push_artifact(Path(f"ROOT_DIR/helm/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        bundler.close_logger()
        pub.close_logger()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"