import inspect
import os
from copy import deepcopy
from pathlib import Path
from threading import _RLock as RLock
import unittest
import urllib
import json
from unittest import mock
import pytest
from test.mock_objects import MockHttpResponse, MockSubprocessRun

from hoppr import HopprContext, Manifest, Result, Sbom
from hoppr.exceptions import HopprError

from nexus_bundler.bundler import NexusBundlePlugin
from nexus_bundler.nexus_instance import NexusInstance
from nexus_bundler.publishers.apt_publisher import AptPublisher

test_fingerprint = b"""
gpg: checking the trustdb
gpg: no ultimately trusted keys found
tru:o:1:123456:1:3:1:5
pub:-:4096:1:1111111111111111:1673275964:::-:::scESC::::::23::0:
fpr:::::::::my_test_fingerprint:
uid:-::::1673275964::2222222222222::my_test_id::::::::::0:
sub:-:4096:1:4444444444444:1673275964::::::e::::::23:
fpr:::::::::5555555555555555555:
"""

class TestAptPublisher(unittest.TestCase):

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.logfile_lock = RLock()

    def get_test_publisher(self):
        instance = NexusInstance(url="http://my-nexus.org", userid="my-userid", password_env="NEXUS_PW")
        manifest = Manifest.load(Path(__file__).parent / "resources" / "manifests" / "apt-manifest.yml")
        sbom_path = Path("test/resources/sboms/int_apt_bom.json")
        bom_dict = json.loads(sbom_path.read_text(encoding="utf-8"))
        sbom = Sbom(**bom_dict)

        context = HopprContext(
            collect_root_dir=Path("ROOT_DIR"),
            consolidated_sbom=sbom,
            credential_required_services=None,
            delivered_sbom=deepcopy(sbom),
            logfile_lock=self.logfile_lock,
            max_processes=3,
            repositories=manifest.repositories,
            sboms=list(Sbom.loaded_sboms.values()),
            stages=[],
        )

        bundler = NexusBundlePlugin(context)
        pub = AptPublisher(instance, context, config={"gpg_command": "gpg"})

        return pub, bundler

    @mock.patch("nexus_bundler.publishers.apt_publisher.open")
    @mock.patch("requests.post", return_value=MockHttpResponse(201, content="mocked content"))
    @mock.patch.object(AptPublisher, "_has_good_repository", return_value=False)
    @mock.patch("nexus_bundler.publishers.base_publisher.check_for_missing_commands", return_value=Result.success())
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_prepare_push_success(self,
            mock_command_check,
            mock_has_good_repo,
            mock_request_post,
            mock_open):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub.push_artifact(Path(f"ROOT_DIR/deb/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        bundler.close_logger()
        pub.close_logger()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @mock.patch.object(AptPublisher, "_generate_gpg_keys", return_value="test_gpg_key")
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_build_repo_request_success(self, mock_gen_keys):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        result = pub._build_repository_request(Path(f"ROOT_DIR/apt/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))
        pub.close_logger()
        bundler.close_logger()

        assert result["apt"] == {"distribution": "bionic", "forceBasicAuth": False}
        assert result["aptSigning"] == {"keypair": "test_gpg_key"}

    @mock.patch.object(AptPublisher, "_generate_gpg_keys", return_value=None)
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_build_repo_request_no_gpgkey(self, mock_gen_keys):

        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        pub, bundler = self.get_test_publisher()

        with pytest.raises(HopprError):
            result = pub._build_repository_request(Path(f"ROOT_DIR/apt/{urllib.parse.quote_plus('/some/repository/theRepoName/other/stuff')}/more"))

        pub.close_logger()
        bundler.close_logger()

    @mock.patch.object(AptPublisher, "run_command", side_effect=[
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=0, stdout=b"test_gpg_key"),
        MockSubprocessRun(rc=0, stdout=test_fingerprint),
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=0),
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_gen_gpgkeys_success(self, mock_subprocess_runS):

            print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

            pub, bundler = self.get_test_publisher()

            result = pub._generate_gpg_keys()

            assert result == "test_gpg_key"

    @mock.patch.object(AptPublisher, "run_command", side_effect=[
        MockSubprocessRun(rc=1),
        MockSubprocessRun(rc=0, stdout=b"test_gpg_key"),
        MockSubprocessRun(rc=0, stdout=test_fingerprint),
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=0),
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_gen_gpgkeys_fail_1(self, mock_subprocess_runS):

            print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

            pub, bundler = self.get_test_publisher()

            result = pub._generate_gpg_keys()

            assert result is None

    @mock.patch.object(AptPublisher, "run_command", side_effect=[
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=1, stdout=b"test_gpg_key"),
        MockSubprocessRun(rc=0, stdout=test_fingerprint),
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=0),
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_gen_gpgkeys_fail_2(self, mock_subprocess_runS):

            print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

            pub, bundler = self.get_test_publisher()

            result = pub._generate_gpg_keys()

            assert result is None

    @mock.patch.object(AptPublisher, "run_command", side_effect=[
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=0, stdout=b"test_gpg_key"),
        MockSubprocessRun(rc=1, stdout=test_fingerprint),
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=0),
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_gen_gpgkeys_fail_3(self, mock_subprocess_runS):

            print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

            pub, bundler = self.get_test_publisher()

            result = pub._generate_gpg_keys()

            assert result is None

    @mock.patch.object(AptPublisher, "run_command", side_effect=[
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=0, stdout=b"test_gpg_key"),
        MockSubprocessRun(rc=0, stdout=test_fingerprint),
        MockSubprocessRun(rc=1),
        MockSubprocessRun(rc=0),
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_gen_gpgkeys_fail_4(self, mock_subprocess_runS):

            print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

            pub, bundler = self.get_test_publisher()

            result = pub._generate_gpg_keys()

            assert result is None

    @mock.patch.object(AptPublisher, "run_command", side_effect=[
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=0, stdout=b"test_gpg_key"),
        MockSubprocessRun(rc=0, stdout=test_fingerprint),
        MockSubprocessRun(rc=0),
        MockSubprocessRun(rc=1),
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_gen_gpgkeys_fail_5(self, mock_subprocess_runS):

            print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

            pub, bundler = self.get_test_publisher()

            result = pub._generate_gpg_keys()

            assert result is None
