from concurrent.futures import Future
import inspect
import os
import json
from copy import deepcopy
from pathlib import Path
from threading import _RLock as RLock
from nexus_bundler.publishers.docker_publisher import DockerPublisher
from nexus_bundler.publishers.maven_publisher import MavenPublisher
import pytest 
import unittest
from unittest import mock

from hoppr import HopprContext, Manifest, Result, Sbom
from hoppr.exceptions import HopprPluginError

from nexus_bundler.bundler import NexusBundlePlugin, _publish
from nexus_bundler.publishers.apt_publisher import AptPublisher
from nexus_bundler.publishers.raw_publisher import RawPublisher
from nexus_bundler.publishers.pypi_publisher import PypiPublisher
from nexus_bundler.publishers.helm_publisher import HelmPublisher
from nexus_bundler.publishers.git_publisher import GitPublisher
from nexus_bundler.publishers.yum_publisher import YumPublisher

class TestBundler(unittest.TestCase):

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.logfile_lock = RLock()

    def get_test_bundler(self):
        manifests_dir = Path(__file__).parent / "resources" / "manifests"
        sbom_path = Path("test/resources/sboms/int_yum_bom.json")
        bom_dict = json.loads(sbom_path.read_text(encoding="utf-8"))
        sbom = Sbom(**bom_dict)

        manifest = Manifest.load(
            {
                "schemaVersion": "v1",
                "kind": "Manifest",
                "metadata": {
                    "name": "Base Publisher Test Manifest",
                    "version": "0.1.0",
                    "description": "A manifest for testing base publisher.",
                },
                "sboms": [],
                "includes": [
                    {"local": manifests_dir / "apt-manifest.yml"},
                    {"local": manifests_dir / "docker-manifest.yml"},
                    {"local": manifests_dir / "git-manifest.yml"},
                    {"local": manifests_dir / "helm-manifest.yml"},
                    {"local": manifests_dir / "maven-manifest.yml"},
                    {"local": manifests_dir / "pypi-manifest.yml"},
                    {"local": manifests_dir / "raw-manifest.yml"},
                    {"local": manifests_dir / "yum-manifest.yml"},
                ],
                "repositories": {},
            }
        )

        context = HopprContext(
            collect_root_dir=Path("ROOT_DIR"),
            consolidated_sbom=sbom,
            credential_required_services=None,
            delivered_sbom=deepcopy(sbom),
            logfile_lock=self.logfile_lock,
            max_processes=3,
            repositories=manifest.repositories,
            sboms=list(Sbom.loaded_sboms.values()),
            stages=[],
        )

        bundler = NexusBundlePlugin(context, config={})

        return bundler

    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_get_version(self):
        my_plugin = self.get_test_bundler()
        assert len(my_plugin.get_version()) > 0

    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_get_publisher(self):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        bundler = self.get_test_bundler()
        bundler.close_logger()

        assert bundler._get_publisher(Path(f"ROOT_DIR/")) is None
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/generic/something/more")), RawPublisher)
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/raw/something/more")), RawPublisher)
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/pypi/something/more")), PypiPublisher)
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/helm/something/more")), HelmPublisher)
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/git/something/more")), GitPublisher)
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/rpm/something/more")), YumPublisher)
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/maven/something/more")), MavenPublisher)
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/docker/something/more")), DockerPublisher)
        assert isinstance(bundler._get_publisher(Path(f"ROOT_DIR/deb/something/more")), AptPublisher)
        
        with pytest.raises(HopprPluginError):
                pub = bundler._get_publisher(Path(f"ROOT_DIR/junk/something/more"))

    @mock.patch.object(Future, "result", return_value = Result.success() )
    @mock.patch.object(RawPublisher, "should_process_at", side_effect=[False, True])
    @mock.patch("os.walk", return_value = [
        ("ROOT_DIR/", ["raw"], []),
        ("ROOT_DIR/raw/", ["my_repo"], []),
        ("ROOT_DIR/raw/my_repo", [], ["file1"])
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_bundle_process_dir(self,
        mock_os_walk,
        mock_raw_process_at,
        mock_future_result
    ):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        bundler = self.get_test_bundler()

        result = bundler.pre_stage_process()
        bundler.close_logger()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @mock.patch.object(Future, "result", return_value = Result.fail("future fail") )
    @mock.patch.object(RawPublisher, "should_process_at", side_effect=[False, False, True])
    @mock.patch("os.walk", return_value = [
        ("ROOT_DIR/", ["raw"], []),
        ("ROOT_DIR/raw/", ["my_repo"], []),
        ("ROOT_DIR/raw/my_repo", [], ["file1"])
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_bundle_process_file_fail(self,
        mock_os_walk,
        mock_raw_process_at,
        mock_future_result
    ):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        bundler = self.get_test_bundler()

        result = bundler.pre_stage_process()
        bundler.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'publish' processes failed"

    @mock.patch.object(Future, "result", return_value = Result.retry("future retry") )
    @mock.patch.object(RawPublisher, "should_process_at", side_effect=[False, False, True])
    @mock.patch("os.walk", return_value = [
        ("ROOT_DIR/", ["raw"], []),
        ("ROOT_DIR/raw/", ["my_repo"], []),
        ("ROOT_DIR/raw/my_repo", [], ["file1"])
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_bundle_process_file_retry(self,
        mock_os_walk,
        mock_raw_process_at,
        mock_future_result
    ):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        bundler = self.get_test_bundler()

        result = bundler.pre_stage_process()
        bundler.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'publish' processes returned 'retry'"

    @mock.patch.object(RawPublisher, "finalize", return_value=Result.fail("finalize fail"))
    @mock.patch.object(Future, "result", return_value = Result.success() )
    @mock.patch.object(RawPublisher, "should_process_at", side_effect=[False, True])
    @mock.patch("os.walk", return_value = [
        ("ROOT_DIR/", ["raw"], []),
        ("ROOT_DIR/raw/", ["my_repo"], []),
        ("ROOT_DIR/raw/my_repo", [], ["file1"])
    ])
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_bundle_process_dir_finalize_fail(self,
        mock_os_walk,
        mock_raw_process_at,
        mock_future_result,
        mock_raw_finalize
    ):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        bundler = self.get_test_bundler()

        result = bundler.pre_stage_process()
        bundler.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'finalize' processes failed"

    @mock.patch.object(RawPublisher, "publish", return_value=Result.fail("publish fail"))
    @mock.patch.dict(os.environ, {"NEXUS_PW": "test-password"})
    def test_publish(self, mock_raw_publish):
        print(f"----- Starting {inspect.currentframe().f_code.co_name} -----")

        bundler = self.get_test_bundler()
        publisher = bundler._get_publisher(Path("ROOT_DIR/raw/my_repo"))

        result = _publish(publisher, Path("test/path"))
        bundler.close_logger()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "publish fail"
