from urllib.error import HTTPError


class MockHttpResponse:
    """
    Mocked version of requests HttpResponse object
    """
    def __init__(self, status_code, reason="", json_data=None, content=None, url="http://mock.url"):
        self.json_data = json_data
        self.status_code = status_code
        self.content = content
        self.reason = reason
        self.text = reason
        self.url = url

    def close(self):
        return self

    def json(self):
        return self.json_data

    def iter_content(self, chunk_size=0):
        return [self.content]

    def raise_for_status(self):
        if self.status_code >= 300:
            raise HTTPError(f"Error code {self.status_code}")

class MockSubprocessRun:
    """
    Mocks a subprocess.run call
    """

    def __init__(self, rc=0, stdout=b"Sample Standard Output", stderr=b"Sample Standard Error"):
        self.returncode = rc
        self.stdout = stdout
        self.stderr = stderr
