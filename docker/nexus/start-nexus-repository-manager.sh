#!/usr/bin/env bash

readonly NEXUS_PASSWORD="admin123"
readonly NEXUS_HOST="localhost:8081"
readonly NEXUS_API_URL="http://$NEXUS_HOST/service/rest/v1"

export NO_PROXY="localhost,local,127.0.0.1"
export no_proxy="$NO_PROXY"

# Run Nexus in background
/opt/sonatype/nexus/bin/nexus run &> /dev/null & disown

curl_opts=(--fail --silent --location --output /dev/null)

counter=0
while true; do
  ((counter++))

  curl_rc="$(curl "${curl_opts[@]}" --write-out "%{response_code}" --url "http://$NEXUS_HOST")"

  echo "Waiting for Nexus to start (attempt #$counter), curl_rc = $curl_rc"
  if [[ $curl_rc == "200" ]]; then break; fi

  if [[ $counter -gt 30 ]]; then
    echo "ERROR: Nexus container did not start properly after a reasonable length of time"
    exit "$counter"
  fi

  sleep 10
done

initial_password="$(cat /nexus-data/admin.password)"

echo "Updating Nexus admin password..."
curl "${curl_opts[@]}" --request PUT \
  --header "Accept: application/json" \
  --header "Content-Type: text/plain" \
  --data "$NEXUS_PASSWORD" --user "admin:$initial_password" \
  --url "$NEXUS_API_URL/security/users/admin/change-password"

echo "Enabling anonymous access..."
curl "${curl_opts[@]}" --request PUT \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --data '{"enabled": true, "userId": "anonymous", "realmName": "NexusAuthorizingRealm"}' \
  --user "admin:$NEXUS_PASSWORD" --url "$NEXUS_API_URL/security/anonymous"

echo "Nexus startup complete."

# Continue looping to keep Nexus running
while true; do sleep 30; done
